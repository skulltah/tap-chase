# Tap Chase

The rules are simple: tap the screen to change direction. Can you beat the game? Can you get to the top of the leaderboard?

Play store: https://play.google.com/store/apps/details?id=io.github.skulltah.colorseek

YouTube: https://www.youtube.com/watch?v=ukNnBeNsts8

There are four types of collectibles:

GREEN makes you bigger

TEAL heals you and gives you a point

PURPLE makes you huge and gives you three points

GOLD makes you invincible for a short amount of time

## Authors

Max Marshall